﻿using System.Linq;
using Clustering.Model;

namespace Clustering.Metrics
{
    public class JaccardDistance : IDistanceMetric
    {
        public double GetDistance(Element firstElement, Element secondElement)
        {
            var interSect = firstElement.DataPoints.Intersect(secondElement.DataPoints).ToList();

            if (!interSect.Any())
                return 1d / double.Epsilon;

            var unionSect = firstElement.DataPoints.Union(secondElement.DataPoints);

            return 1d / (((double)interSect.Count() / unionSect.Count()) + double.Epsilon);
        }
    }
}