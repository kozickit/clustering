﻿using System;
using Clustering.Model;

namespace Clustering.Metrics
{
    public class EuclideanDistance : IDistanceMetric
    {
        public double GetDistance(Element firstElement, Element secondElement)
        {
            var dataPoints1 = firstElement.DataPoints;
            var dataPoints2 = secondElement.DataPoints;

            if (dataPoints1.Count != dataPoints2.Count)
                throw new ArgumentException("Input vectors must be of the same dimension.");

            var sum = 0.0;

            for (var i = 0; i < dataPoints1.Count; i++)
            {
                sum += Math.Pow((int) dataPoints1[i] - (int) dataPoints2[i], 2);
            }

            return Math.Sqrt(sum);
        }
    }
}