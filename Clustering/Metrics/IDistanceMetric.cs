﻿using Clustering.Model;

namespace Clustering.Metrics
{
    public interface IDistanceMetric
    {
        double GetDistance(Element firstElement, Element secondElement);
    }
}