﻿using System.Collections.Generic;

namespace Clustering.Model
{
    public class Cluster
    {
        public List<Element> Elements { get; set; }
        public Element Clustroid { get; set; }
        public Element OldClustroid { get; set; }

        public Cluster()
        {
            Elements = new List<Element>();
        }
    }
}