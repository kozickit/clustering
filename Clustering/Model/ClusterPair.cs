﻿namespace Clustering.Model
{
    public class ClusterPair
    {
        public Cluster Cluster1 { get; set; }
        public Cluster Cluster2 { get; set; }
        public double Distance { get; set; }

        public ClusterPair(Cluster cluster1, Cluster cluster2, double distance)
        {
            Cluster1 = cluster1;
            Cluster2 = cluster2;
            Distance = distance;
        }

        public bool HasCluster(Cluster cluster)
        {
            return Cluster1 == cluster || Cluster2 == cluster;
        }
    }
}