﻿using System.Collections.Generic;

namespace Clustering.Model
{
    /// <summary>
    /// An element consists of an array of data points. To use single values (e.g. to cluster numbers) add that value 
    /// as the sole data point to an element object.
    /// </summary>
    public class Element
    {
        /// <summary>
        /// Returns the id of the element.
        /// </summary>
        public int Id { get; set; }
        public List<object> DataPoints { get; set; }

        public Element()
        {
            DataPoints = new List<object>();
        }

        public override string ToString()
        {
            var str = Id + " ";
            foreach (var point in DataPoints)
            {
                str += " pos: " + point.ToString();
            }
            return str;
        }
    }
}