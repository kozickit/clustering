﻿using System.Collections.Generic;

namespace Clustering.Model
{
    public class ClusterPairs
    {
        public HashSet<ClusterPair> Pairs { get; set; }

        public ClusterPairs()
        {
            Pairs = new HashSet<ClusterPair>();
        }

        public void Add(ClusterPair clusterPair)
        {
            Pairs.Add(clusterPair);
        }

        public ClusterPair LowestDistancePair
        {
            get
            {
                ClusterPair lowestDistancePair = null;

                foreach (var pair in Pairs)
                {
                    if (lowestDistancePair == null || lowestDistancePair.Distance > pair.Distance)
                        lowestDistancePair = pair;
                }

                return lowestDistancePair;
            }
        }

        public void RemovePairsByOldClusters(Cluster cluster1, Cluster cluster2)
        {
            var toRemove = new List<ClusterPair>();

            foreach (var pair in Pairs)
            {
                if (pair.HasCluster(cluster1) || pair.HasCluster(cluster2))
                {
                    toRemove.Add(pair);
                }
            }
            foreach (var pair in toRemove)
            {
                Pairs.Remove(pair);
            }
        }
    }
}