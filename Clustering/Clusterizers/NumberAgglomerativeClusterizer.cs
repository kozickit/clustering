﻿using System.Collections.Generic;
using Clustering.Model;

namespace Clustering.Clusterizers
{
    /// <summary>
    /// Implements the cluster algorithm by stopping it as soon as a specified count of clusters is reached.
    /// </summary>
    public class NumberAgglomerativeClusterizer : AbstractAgglomerativeClusterizer
    {
        private readonly int _desiredClusterCount;

        /// <summary>
        /// Creates a new instance of the NumberCriterionAlgorithm. 
        /// </summary>
        /// <param name="desiredClusterCount">The count of clusters to build.</param>
        public NumberAgglomerativeClusterizer(int desiredClusterCount)
        {
            _desiredClusterCount = desiredClusterCount;
        }

        protected override bool IsFinished(List<Cluster> currentClusters, ClusterPair lowestDistancePair)
        {
            return _desiredClusterCount < 1 || _desiredClusterCount >= currentClusters.Count;
        }
    }
}