﻿using System.Collections.Generic;
using Clustering.Fusions;
using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Clusterizers
{
    public interface IAgglomerativeClusterizer
    {
        List<Cluster> GetClusters(List<Element> elements, ILinkage linkage, IDistanceMetric distanceMetric);
    }
}