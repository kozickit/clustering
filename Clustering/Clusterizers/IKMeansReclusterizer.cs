﻿using System.Collections.Generic;
using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Clusterizers
{
    public interface IKMeansReclusterizer
    {
        List<Cluster> GetClusters(List<Cluster> clusters, IDistanceMetric distanceMetric);
    }
}