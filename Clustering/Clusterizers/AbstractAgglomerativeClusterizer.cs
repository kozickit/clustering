﻿using System.Collections.Generic;
using Clustering.Fusions;
using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Clusterizers
{
    /// <summary>
    /// Abstract class to implement the generic part of the algorithm. Subclasses implement the stop condition to stop the algorithm.
    /// </summary>
    public abstract class AbstractAgglomerativeClusterizer : IAgglomerativeClusterizer
    {
        /// <summary>
        /// Starts the clustering.
        /// </summary>
        /// <param name="elements"></param>
        /// <param name="linkage"></param>
        /// <param name="distanceMetric"></param>
        /// <returns></returns>
        public List<Cluster> GetClusters(List<Element> elements, ILinkage linkage, IDistanceMetric distanceMetric)
        {
            var clusters = new List<Cluster>();
            var pairs = new ClusterPairs();

            // 1. Initialize each element as a cluster
            foreach (var el in elements)
            {
                var cl = new Cluster();
                cl.Elements.Add(el);
                clusters.Add(cl);
            }

            // 2. a) Calculate the distances of all clusters to all other clusters
            for (var i1 = 0; i1 < clusters.Count; i1++)
            {
                for (var i2 = i1 + 1; i2 < clusters.Count; i2++)
                {
                    var cl1 = clusters[i1];
                    var cl2 = clusters[i2];

                    var pair = new ClusterPair(cl1, cl2, linkage.CalculateDistance(cl1, cl2, distanceMetric));
                    pairs.Add(pair);
                }
            }

            // 2. b) Initialize the pair with the lowest distance to each other.
            var lowestDistancePair = pairs.LowestDistancePair;

            // 3. Merge clusters to new clusters and recalculate distances in a loop until there are only countCluster clusters
            while (!IsFinished(clusters, lowestDistancePair))
            {
                // a) Merge: Create a new cluster and add the elements of the two old clusters                
                lowestDistancePair = pairs.LowestDistancePair;
                var newCluster = new Cluster();
                newCluster.Elements.AddRange(lowestDistancePair.Cluster1.Elements);
                newCluster.Elements.AddRange(lowestDistancePair.Cluster2.Elements);

                // b) Remove the two old clusters from clusters
                clusters.Remove(lowestDistancePair.Cluster1);
                clusters.Remove(lowestDistancePair.Cluster2);

                // c) Remove the two old clusters from pairs
                pairs.RemovePairsByOldClusters(lowestDistancePair.Cluster1, lowestDistancePair.Cluster2);

                // d) Calculate the distance of the new cluster to all other clusters and save each as pair
                foreach (var cluster in clusters)
                {
                    var pair = new ClusterPair(cluster, newCluster, linkage.CalculateDistance(cluster, newCluster, distanceMetric));
                    pairs.Add(pair);
                }

                // e) Add the new cluster to clusters
                clusters.Add(newCluster);
            }

            return clusters;
        }

        /// <summary>
        /// Checks if the algorithm has to stop.
        /// </summary>
        /// <param name="currentClusters"></param>
        /// <param name="lowestDistancePair"></param>
        /// <returns></returns>
        protected abstract bool IsFinished(List<Cluster> currentClusters, ClusterPair lowestDistancePair);
    }
}