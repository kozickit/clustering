﻿using System.Collections.Generic;
using System.Linq;
using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Clusterizers
{
    public class KMeansReclusterizer : IKMeansReclusterizer
    {
        public List<Cluster> GetClusters(List<Cluster> clusters, IDistanceMetric distanceMetric)
        {
            var allElements = new List<Element>().ToList();
            foreach (var cluster in clusters)
            {
                allElements.AddRange(cluster.Elements);
            }

            do
            {
                foreach (var cluster in clusters)
                {
                    Element candidateClustroid = null;
                    var minSumOfDistances = double.MaxValue;

                    foreach (var element1 in cluster.Elements)
                    {
                        double sumOfDistances = 0;

                        foreach (var element2 in cluster.Elements)
                        {
                            if (element1 != element2)
                            {
                                sumOfDistances += distanceMetric.GetDistance(element1, element2);
                            }
                        }
                        if (sumOfDistances < minSumOfDistances)
                        {
                            candidateClustroid = element1;
                            minSumOfDistances = sumOfDistances;
                        }
                    }

                    cluster.OldClustroid = cluster.Clustroid;
                    cluster.Clustroid = candidateClustroid;
                    cluster.Elements.Clear();
                }

                foreach (var element in allElements)
                {
                    var nearestCluster = clusters.First();
                    var minDistance = double.MaxValue;

                    foreach (var cluster in clusters)
                    {
                        var distance = distanceMetric.GetDistance(element, cluster.Clustroid);

                        if (distance < minDistance)
                        {
                            nearestCluster = cluster;
                            minDistance = distance;
                        }
                    }
                    nearestCluster.Elements.Add(element);
                }
            } while (!IsFinished(clusters));

            return clusters;
        }

        private bool IsFinished(List<Cluster> clusters)
        {
            foreach (var cluster in clusters)
            {
                if (cluster.Clustroid != cluster.OldClustroid)
                {
                    return false;
                }
            }
            return true;
        }
    }
}