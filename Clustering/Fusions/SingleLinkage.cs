﻿using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Fusions
{
    public class SingleLinkage : ILinkage
    {
        public double CalculateDistance(Cluster firstCluster, Cluster secondCluster, IDistanceMetric distanceMetric)
        {
            var minDistance = double.MaxValue;

            foreach (var element1 in firstCluster.Elements)
            {
                foreach (var element2 in secondCluster.Elements)
                {
                    var distance = distanceMetric.GetDistance(element1, element2);
                    if (distance < minDistance)
                        minDistance = distance;
                }
            }

            return minDistance;
        }
    }
}