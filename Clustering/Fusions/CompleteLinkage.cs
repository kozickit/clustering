﻿using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Fusions
{
    public class CompleteLinkage : ILinkage
    {
        public double CalculateDistance(Cluster firstCluster, Cluster secondCluster, IDistanceMetric distanceMetric)
        {
            var maxDistance = double.MinValue;

            foreach (var element1 in firstCluster.Elements)
            {
                foreach (var element2 in secondCluster.Elements)
                {
                    var distance = distanceMetric.GetDistance(element1, element2);
                    if (distance > maxDistance)
                        maxDistance = distance;
                }
            }

            return maxDistance;
        }
    }
}