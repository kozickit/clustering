﻿using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Fusions
{
    public interface ILinkage
    {
        double CalculateDistance(Cluster firstCluster, Cluster secondCluster, IDistanceMetric distanceMetric);
    }
}