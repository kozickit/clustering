﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Clustering.Clusterizers;
using Clustering.Fusions;
using Clustering.Metrics;
using Clustering.Model;

namespace Clustering.Application
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var elements = new List<Element>();

            var array = File.ReadAllLines(args[0]);

            for (int i = 1; i < array.Length; i++)
            {
                var line = array[i];
                var values = line.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                var element = new Element {Id = i + 1};
                for (var j = 0; j < 2; j++)
                {
                    element.DataPoints.Add(int.Parse(values[j]));
                }

                elements.Add(element);
            }

            List<Cluster> clusters;
            List<Cluster> reclusters;

            IAgglomerativeClusterizer agglomerativeClusterizer;
            IKMeansReclusterizer kMeansReclusterizer;

            ILinkage linkage = new CompleteLinkage();
            IDistanceMetric distanceMetric = new EuclideanDistance();

            Console.WriteLine("\nAgglomerative clustering...\n");
            agglomerativeClusterizer = new DistanceAgglomerativeClusterizer(int.Parse(args[1]), int.Parse(args[2]));
            clusters = agglomerativeClusterizer.GetClusters(elements, linkage, distanceMetric);
            PrintResult(clusters, "Agglomerative_clustering.bmp");
            
            Console.WriteLine("\nK-means reclustering...\n");
            kMeansReclusterizer = new KMeansReclusterizer();
            reclusters = kMeansReclusterizer.GetClusters(clusters, distanceMetric);
            PrintResult(reclusters, "Kmeans_reclustering.bmp");
            
            Console.WriteLine("\nDone.");
        }

        private static void PrintResult(List<Cluster> clusters, string outputFile)
        {
            var bitmap = new Bitmap(1000, 1000);
            var brushes = new List<Brush>
            {
                Brushes.Red,
                Brushes.Green,
                Brushes.Blue,
                Brushes.DeepPink,
                Brushes.Brown,
                Brushes.Purple,
                Brushes.SpringGreen,
                Brushes.Orange,
                Brushes.Magenta,
                Brushes.Aqua

            };
            using(var g = Graphics.FromImage(bitmap))
            {
                g.FillRectangle(Brushes.White, 0, 0, 1000, 1000);
                for (var i = 0; i < clusters.Count; i++)
                {
                    var cluster = clusters[i];
                    var currentBrush = brushes[i];

                    foreach (var element in cluster.Elements)
                    {
                        g.FillRectangle(currentBrush, (int) element.DataPoints[0] - 1, (int) element.DataPoints[1] - 1,
                            3, 3);
                    }
                }
            }
            bitmap.Save(outputFile);
        }
    }
}
